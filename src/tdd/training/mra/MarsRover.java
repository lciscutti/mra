package tdd.training.mra;

import java.util.ArrayList;
import java.util.List;

public class MarsRover {

	private int planetX;
	private int planetY;
	private List<String> planetObstacles;
	private List<String> planet;
	private String[] roverStatus = { "0", "0", "N"};
	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		this.planetX = planetX;
		this.planetY = planetY;
		this.planetObstacles = planetObstacles;

		planet = new ArrayList<>();

		for (int i = 0; i < planetX; i++) {
			for (int j = 0; j < planetY; j++) {
				if (planetObstacles.contains("(" + i + "," + j + ")")) {
					planet.add("(oi_" + i + ",oi_" + j + ")");
				} else
					planet.add("(" + i + "," + j + ")");
			}
		}
	}

	public int getPlanetX() {
		return planetX;
	}

	public int getPlanetY() {
		return planetY;
	}

	public List<String> getPlanetObstacles() {
		return planetObstacles;
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) {
		return planet.contains("(oi_" + x + ",oi_" + y + ")");
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) {
		StringBuilder obstacles = new StringBuilder("");
		if (commandString == "") {
			return roverStatusToString();
		} else {

			for (int i = 0; i < commandString.length(); i++) {
				String[] temp = roverStatus;
				
				if (commandString.charAt(i) == 'r') {
					if (roverStatus[2] == "N") {
						roverStatus[2] = ("E");
					} else if (roverStatus[2] == "E") {
						roverStatus[2] = ("S");
					} else if (roverStatus[2] == "S") {
						roverStatus[2] = ("W");
					} else if (roverStatus[2] == "W") {
						roverStatus[2] = ("N");
					}
					
				} else if (commandString.charAt(i) == 'l') {
					if (roverStatus[2] == "N") {
						roverStatus[2] = ("W");
					} else if (roverStatus[2] == "W") {
						roverStatus[2] = ("S");
					} else if (roverStatus[2] == "S") {
						roverStatus[2] = ("E");
					} else if (roverStatus[2] == "E") {
						roverStatus[2] = ("N");
					}
					
				} else if (commandString.charAt(i) == 'f') {
					if (roverStatus[2] == "N") {
						int value = returnNewValue("+",Integer.valueOf(roverStatus[1]),"y");
						roverStatus[1] = String.valueOf(value);
						if(planetContainsObstacleAt(Integer.valueOf(roverStatus[0]), Integer.valueOf(roverStatus[1]))) {
							obstacles.append(roverStatusToString());
							roverStatus=temp;
						}
					} else if (roverStatus[2] == "E") {
						int value = returnNewValue("+",Integer.valueOf(roverStatus[0]),"x");
						roverStatus[0] = String.valueOf(value);
						if(planetContainsObstacleAt(Integer.valueOf(roverStatus[0]), Integer.valueOf(roverStatus[1]))) {
							obstacles.append(roverStatusToString());
							roverStatus=temp;
						}
					} else if (roverStatus[2] == "S") {
						int value = returnNewValue("-",Integer.valueOf(roverStatus[1]),"y");
						roverStatus[1] = String.valueOf(value);
						if(planetContainsObstacleAt(Integer.valueOf(roverStatus[0]), Integer.valueOf(roverStatus[1]))) {
							obstacles.append(roverStatusToString());
							roverStatus=temp;
						}
					} else if (roverStatus[2] == "W") {
						int value = returnNewValue("-",Integer.valueOf(roverStatus[0]),"x");
						roverStatus[0] = String.valueOf(value);
						if(planetContainsObstacleAt(Integer.valueOf(roverStatus[0]), Integer.valueOf(roverStatus[1]))) {
							obstacles.append(roverStatusToString());
							roverStatus=temp;
						}
					}
					
				} else if (commandString.charAt(i) == 'b') {
					if (roverStatus[2] == "N") {
						int value = returnNewValue("-",Integer.valueOf(roverStatus[1]),"y");
						roverStatus[1] = String.valueOf(value);
						if(planetContainsObstacleAt(Integer.valueOf(roverStatus[0]), Integer.valueOf(roverStatus[1]))) {
							obstacles.append(roverStatusToString());
							roverStatus=temp;
						}
					} else if (roverStatus[2] == "E") {
						int value = returnNewValue("-",Integer.valueOf(roverStatus[0]),"x");
						roverStatus[0] = String.valueOf(value);
						if(planetContainsObstacleAt(Integer.valueOf(roverStatus[0]), Integer.valueOf(roverStatus[1]))) {
							obstacles.append(roverStatusToString());
							roverStatus=temp;
						}
					} else if (roverStatus[2] == "S") {
						int value = returnNewValue("+",Integer.valueOf(roverStatus[1]),"y");
						roverStatus[1] = String.valueOf(value);
						if(planetContainsObstacleAt(Integer.valueOf(roverStatus[0]), Integer.valueOf(roverStatus[1]))) {
							obstacles.append(roverStatusToString());
							roverStatus=temp;
						}
					} else if (roverStatus[2] == "W") {
						int value = returnNewValue("+",Integer.valueOf(roverStatus[0]),"x");
						roverStatus[0] = String.valueOf(value);
						if(planetContainsObstacleAt(Integer.valueOf(roverStatus[0]), Integer.valueOf(roverStatus[1]))) {
							obstacles.append(roverStatusToString());
							roverStatus=temp;
						}
					}

					
				}
			}
		}

		return roverStatusToString()+obstacles.toString();

	}
	
	
	private int returnNewValue(String operator, int value, String asse) {
		if(operator.equals("+")) {
			if (asse.equals("x")) {
				if(value==planetX-1) {
					value=0;
				}else value++;
			}else {
				if(value==planetY-1) {
					value=0;
				}else value++;
			}
		}else {
			if (asse.equals("x")) {
				if(value==0) {
					value=planetX-1;
				}else value--;
			}else {
				if(value==0) {
					value=planetY-1;
				}else value--;
			}
		}
		
		return  value;
	}


	public String roverStatusToString() {
		return "(" + roverStatus[0] + "," + roverStatus[1] + "," + roverStatus[2] + ")";
	}
	
	

}
