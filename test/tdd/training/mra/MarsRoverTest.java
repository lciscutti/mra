package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {
	
	@Test
	public void testMarsRoverPlanetCreation() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,1)");
		planetObstacles.add("(2,2)");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		assertEquals(10,rover.getPlanetX());
		assertEquals(12,rover.getPlanetY());
		assertEquals(planetObstacles,rover.getPlanetObstacles());
		assertTrue(rover.planetContainsObstacleAt(0, 1));
	}

	@Test
	public void testRoverStatusForEmptyString() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		
		assertEquals("(0,0,N)",rover.executeCommand(""));
	}
	
	@Test
	public void testRoverStatusForTurningRightFromStartPosition() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		
		assertEquals("(0,0,E)",rover.executeCommand("r"));
	}
	
	@Test
	public void testRoverStatusForTurningLeftFromStartPosition() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		
		assertEquals("(0,0,W)",rover.executeCommand("l"));
	}
	
	@Test
	public void testRoverStatusForMovingForwardFromStartPosition() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		
		assertEquals("(0,1,N)",rover.executeCommand("f"));
	}
	
	@Test
	public void testRoverStatusForMovingBackwardFrom01Position() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		rover.executeCommand("f");
		assertEquals("(0,0,N)",rover.executeCommand("b"));
	}
	
	@Test
	public void testRoverStatusForMovingBackwardFrom77Position() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		rover.executeCommand("r");
		rover.executeCommand("f");
		rover.executeCommand("f");
		rover.executeCommand("f");
		rover.executeCommand("f");
		rover.executeCommand("f");
		rover.executeCommand("f");
		rover.executeCommand("f");
		rover.executeCommand("l");
		rover.executeCommand("f");
		rover.executeCommand("f");
		rover.executeCommand("f");
		rover.executeCommand("f");
		rover.executeCommand("f");
		rover.executeCommand("f");
		rover.executeCommand("f");
		assertEquals("(7,6,N)",rover.executeCommand("b"));
	}
	
	@Test
	public void testRoverStatusForMovingCombinedPosition() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
	
		assertEquals("(2,2,E)",rover.executeCommand("ffrff"));
	}
	
	@Test
	public void testRoverStatusForWrappingPosition() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
	
		assertEquals("(0,9,N)",rover.executeCommand("b"));
	}
	
	@Test
	public void testRoverStatusWithEncountredObstacles() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		planetObstacles.add("(2,2)");
		assertEquals("(1,2,E)(2,2)",rover.executeCommand("ffrfff"));
	}
}
